#  -*- coding: utf-8 -*-
# This file is part health_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .health_snomed_ct import *
from .wizard import *


def register():
    Pool.register(
        SnomedConfig,
        PatientEvaluation,
        SnomedEvaluation,
        LoadSnomedStart,
        LoadSnomedResult,
        LoadSnomedResultLine,
        module='health_snomed_ct', type_='model')
    Pool.register(
        LoadSnomed,
        module='health_snomed_ct', type_='wizard')
