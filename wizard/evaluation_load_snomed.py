# -*- coding: utf-8 -*-
# This file is part health_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction
from ..snomed import SnomedService

__all__ = ['LoadSnomedStart', 'LoadSnomedResult', 'LoadSnomedResultLine',
    'LoadSnomed']


class LoadSnomedStart(ModelView):
    'Load Snomed Start'
    __name__ = 'gnuhealth.patient.evaluation.snomed.load.start'

    search_value = fields.Char('Search')
    page_limit = fields.Selection([
        (5, '5'),
        (10, '10'),
        (20, '20'),
        (50, '50'),
        ], 'Lines to show', sort=False,
        help='Quantity of lines to show at a time')
    offset = fields.Integer('Offset')
    lines = fields.One2Many(
        'gnuhealth.patient.evaluation.snomed.load.result.line',
        'load_snomed_start', 'Diagnosis', readonly=True)


class LoadSnomedResult(ModelView):
    'Load Snomed Result'
    __name__ = 'gnuhealth.patient.evaluation.snomed.load.result'

    search_value = fields.Char('Results for', readonly=True)
    nav_legend = fields.Char('Found', readonly=True)
    total = fields.Integer('Total')
    lines = fields.One2Many(
        'gnuhealth.patient.evaluation.snomed.load.result.line',
        'load_snomed', 'Diagnosis')

    @classmethod
    def __setup__(cls):
        super(LoadSnomedResult, cls).__setup__()
        cls._error_messages.update({
            'concept_already_loaded': 'Concept already loaded',
        })

    @fields.depends('lines')
    def on_change_lines(self):
        concepts = [line.concept_id for line in self.lines]
        warning = self.check_concepts(concepts)
        if warning:
            self.raise_user_error(warning)

    @staticmethod
    def check_concepts(concepts):
        if len(concepts) > len(set(concepts)):
            return 'concept_already_loaded'
        return None


class LoadSnomedResultLine(ModelView):
    'Load Snomed Result Lines'
    __name__ = 'gnuhealth.patient.evaluation.snomed.load.result.line'

    load_snomed = fields.Many2One(
        'gnuhealth.patient.evaluation.snomed.load.result',
        'Load Snomed', required=True, ondelete='CASCADE')
    load_snomed_start = fields.Many2One(
        'gnuhealth.patient.evaluation.snomed.load.start',
        'Load Snomed')
    concept_id = fields.Char('Concept ID', required=True)
    description = fields.Char('Description', required=True)
    preferred_term = fields.Char('Preferred term')
    selected = fields.Boolean('Selected',
        help='Check to include item in the diagnosis')


class LoadSnomed(Wizard):
    'Load Snomed'
    __name__ = 'gnuhealth.patient.evaluation.snomed.load'

    start_state = 'start'
    start = StateView('gnuhealth.patient.evaluation.snomed.load.start',
        'health_snomed_ct.load_snomed_start_view_form', [
            Button('Search', 'result', 'tryton-search', default=True),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    result = StateView('gnuhealth.patient.evaluation.snomed.load.result',
        'health_snomed_ct.load_snomed_result_view_form', [
            Button('Search again', 'start', 'tryton-search', default=True),
            Button('Next', 'result_fwd', 'tryton-forward'),
            Button('Load', 'load', 'tryton-save'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    result_bck = StateView('gnuhealth.patient.evaluation.snomed.load.result',
        'health_snomed_ct.load_snomed_result_view_form', [
            Button('Search again', 'start', 'tryton-search', default=True),
            Button('Go back', 'result_bck', 'tryton-back'),
            Button('Next', 'result_fwd', 'tryton-forward'),
            Button('Load', 'load', 'tryton-save'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    result_fwd = StateView('gnuhealth.patient.evaluation.snomed.load.result',
        'health_snomed_ct.load_snomed_result_view_form', [
            Button('Search again', 'start', 'tryton-search', default=True),
            Button('Go back', 'result_bck', 'tryton-back'),
            Button('Next', 'result_fwd', 'tryton-forward'),
            Button('Load', 'load', 'tryton-save'),
            Button('Cancel', 'end', 'tryton-cancel'),
            ])
    load = StateTransition()

    def default_start(self, fields):
        page_limit = getattr(self.start, 'page_limit', None)
        if page_limit:
            return {'page_limit': page_limit}
        else:
            return {'page_limit': 20}

    def default_result(self, fields):
        if not self.start.offset:
            self.start.offset = 0
        return self.process_result()

    def default_result_bck(self, fields):
        if self.start.offset < self.start.page_limit:
            self.start.offset = 0
        else:
            self.start.offset -= self.start.page_limit
        return self.process_result()

    def default_result_fwd(self, fields):
        self.start.offset += self.start.page_limit
        if self.start.offset > self.result.total:
            self.start.offset -= self.start.page_limit
        return self.process_result()

    def process_result(self):
        params = {
            'activeFilter': 'true',
            'term': self.start.search_value,
            'ecl': '<<404684003 |hallazgo clinico (hallazgo)| '
                'OR <272379006 |Event (event)| OR <243796009 '
                '|Situation with explicit context (situation)| '
                'OR <48176007 |Social context (social concept)|'
            }
        result = SnomedService.get_data(
            section='MAIN/concepts', params=params,
            limit=self.start.page_limit, offset=self.start.offset)
        data = []
        if result:
            for i in result['items']:
                data.append({
                    'concept_id': i['conceptId'],
                    'description': i['fsn'] and i['fsn']['term'] or '',
                    'preferred_term': i['pt'] and i['pt']['term'] or ''
                    })
        total = result and result['total'] or 0
        up_to = self.start.offset + self.start.page_limit \
            if (self.start.offset + self.start.page_limit) < total else total
        offset_str = str(self.start.offset + 1) + '-' + str(up_to)
        found = offset_str + ' de ' + str(total)
        return {
            'search_value': self.start.search_value,
            'nav_legend': found,
            'total': total,
            'lines': data,
            }

    def transition_load(self):
        pool = Pool()
        SnomedDiagnosis = pool.get('gnuhealth.patient.evaluation.snomed')

        load_lines = []
        lines = [line for line in self.result.lines
            if line.selected is True and line.concept_id]
        bck_lines = getattr(self.result_bck, 'lines', None)
        if bck_lines:
            lines.extend([line for line in bck_lines
                if line.selected is True and
                    not any(True for elem in lines
                    if elem.concept_id == line.concept_id)])
        fwd_lines = getattr(self.result_fwd, 'lines', None)
        if fwd_lines:
            lines.extend([line for line in fwd_lines
                if line.selected is True and
                    not any(True for elem in lines
                    if elem.concept_id == line.concept_id)])
        for line in lines:
            data = {
                'evaluation': Transaction().context['active_id'],
                'concept_id': line.concept_id,
                'description': line.description,
                'preferred_term': line.preferred_term,
                }
            load_lines.append(data)

        if load_lines:
            SnomedDiagnosis.create(load_lines)
        return 'end'
