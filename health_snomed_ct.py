# -*- coding: utf-8 -*-
# This file is part health_snomed_ct module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.

from trytond.model import fields, ModelSQL, ModelView, ModelSingleton, Unique
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Not, Bool, Or
from .snomed import SnomedService

__all__ = ['SnomedConfig', 'PatientEvaluation', 'SnomedEvaluation']


class SnomedConfig(ModelSingleton, ModelSQL, ModelView):
    'Snomed Configuration'
    __name__ = 'gnuhealth.snomed.config'

    url = fields.Char('SnomedCT Server URL', required=True,
        help='Base URL to access the Snomed Server')
    iso_lang = fields.Char('ISO Language code', required=True,
        help='ISO 639-1 Language code to use')
    app_id = fields.Char('App ID', help='ID to use in a production server')
    app_key = fields.Char('App Key', help='Key to use in a production server')

    @staticmethod
    def default_url():
        return 'https://snowstorm.msal.gob.ar'

    @staticmethod
    def default_iso_lang():
        return 'es'


class PatientEvaluation(metaclass=PoolMeta):
    __name__ = 'gnuhealth.patient.evaluation'

    snomed_diagnosis = fields.One2Many(
        'gnuhealth.patient.evaluation.snomed',
        'evaluation', 'Snomed diagnosis',
        states={'invisible': Bool(Eval('use_cie10_cepsap'))},
        depends=['use_cie10_cepsap'])
    use_cie10_cepsap = fields.Boolean('Use ICD-10 / CEPS-AP',
        help='Check to use ICD-10 or CEPS-AP diagnosis instead of Snomed')

    @classmethod
    def __setup__(cls):
        super(PatientEvaluation, cls).__setup__()
        cls.diagnosis_domain.states['invisible'] = Or(
            cls.diagnosis_domain.states.get('invisible', False),
            Not(Bool(Eval('use_cie10_cepsap'))))
        if 'use_cie10_cepsap' not in cls.diagnosis_domain.depends:
            cls.diagnosis_domain.depends.append('use_cie10_cepsap')
        cls.diagnosis.states['invisible'] = Or(
            cls.diagnosis.states.get('invisible', False),
            Not(Bool(Eval('use_cie10_cepsap'))))
        if 'use_cie10_cepsap' not in cls.diagnosis.depends:
            cls.diagnosis.depends.append('use_cie10_cepsap')

        cls._buttons.update({
                'load_snomed_diagnosis': {
                    'readonly': ~Eval('state').in_(['in_progress']),
                    'invisible': Bool(Eval('use_cie10_cepsap'))
                    },
                })

    @staticmethod
    def default_use_cie10_cepsap():
        return False

    @classmethod
    @ModelView.button_action('health_snomed_ct.wizard_load_snomed')
    def load_snomed_diagnosis(cls, evaluations):
        pass


class SnomedEvaluation(ModelSQL, ModelView):
    'Snomed Evaluation'
    __name__ = 'gnuhealth.patient.evaluation.snomed'

    evaluation = fields.Many2One('gnuhealth.patient.evaluation',
        'Patient', required=True)
    concept_id = fields.Char('Concept ID', required=True, readonly=True)
    description = fields.Char('Description', required=True, readonly=True)
    preferred_term = fields.Char('Preferred term', readonly=True)
    icd_code = fields.Char('ICD code', readonly=True)

    @classmethod
    def __setup__(cls):
        super(SnomedEvaluation, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('concept_id_uniq', Unique(t, t.evaluation, t.concept_id),
                'The concept must be unique for each evaluation'),
        ]
        cls._buttons.update({
            'map_icd': {'invisible': Eval('icd_code')}
            })

    @classmethod
    @ModelView.button
    def map_icd(cls, evaluations):
        if evaluations:
            params = {
                'referenceSet': '447562003',  # ICD-10 set code
                'referencedComponentId': evaluations[0].concept_id,
                'active': 'true',
                }
            result = SnomedService.get_data(
                section='browser/MAIN/members', params=params)
            if result and result.get('items'):
                item = result['items'][0]
                if 'additionalFields' in item:
                    icd_code = item['additionalFields'].get('mapTarget')
                    icd_code = icd_code and icd_code or '-'
                    if icd_code:
                        cls.write(evaluations, {
                            'icd_code': icd_code})
